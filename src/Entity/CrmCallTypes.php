<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * CrmCallTypes
 *
 * @ORM\Entity(repositoryClass="App\Repository\CrmCallTypesRepository")
 */
class CrmCallTypes
{
    /**
     * @var int
     *
     * @ORM\Column(name="record_id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var \Dispositions
     *
     * @ORM\ManyToOne(targetEntity="Dispositions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_disposition", referencedColumnName="record_id")
     * })
     */
    private $parentDisposition;

    public function getRecordId(): ?string
    {
        return $this->recordId;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getParentDisposition(): ?Dispositions
    {
        return $this->parentDisposition;
    }

    public function setParentDisposition(?Dispositions $parentDisposition): self
    {
        $this->parentDisposition = $parentDisposition;

        return $this;
    }


}

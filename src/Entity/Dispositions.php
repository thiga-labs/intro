<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dispositions
 *
 * @ORM\Entity(repositoryClass="App\Repository\DispositionsRepository")
 */
class Dispositions
{
    /**
     * @var int
     *
     * @ORM\Column(name="record_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $recordId;

    /**
     * @var string
     *
     * @ORM\Column(name="disposition_name", type="text", length=65535, nullable=false)
     */
    private $dispositionName;

    public function getRecordId(): ?int
    {
        return $this->recordId;
    }

    public function getDispositionName(): ?string
    {
        return $this->dispositionName;
    }

    public function setDispositionName(string $dispositionName): self
    {
        $this->dispositionName = $dispositionName;

        return $this;
    }


}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use League\Csv\Reader;
use League\Csv\Writer;
use League\Csv\Utility;

use App\Entity\CrmCallTypes;
use App\Entity\Dispositions;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        //load the CSV document from a file path
        $csv = Reader::createFromPath('1627885890667.csv', 'r');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        $row_datas = $csv->getContent(); //returns the CSV document as a string

        $finito = array();
        foreach($records as $row_data):
            var_dump($row_data);
            echo '<br>';
            echo '<br>';

            $call_type = $row_data["Call Type"];
            $call_disposition = $this->disposition_classification($call_type);
            
            $call_disposition_class = $this->get_disposition($call_disposition);

           // $to_push = [$call_disposition_class];
            $to_push = array('Disposition'=>$call_disposition_class);
            //$row_data['Disposition']=$call_disposition_class;

            $row_data=array_merge($row_data, $to_push);
            
            var_dump($row_data);
            echo '<br>';
            //exit;
            array_push($finito, $row_data);
           // $finito=array_merge($finito, $row_data);
            //$finito[] = array($row_data);
            echo '<h3 style = "color:blue">NXT ROW</h3>';
            echo '<br>';
            echo '<br>';
            unset($row_data);
        endforeach;
        echo '<h3 style = "color:green">FINITO DATA</h3>';
        echo '<br>';
        echo '<br>';
        var_dump($finito);
        array_push($header, "Inquiry");

        $recordz = [
            $header,
            $finito[0],
            // ['foo', 'bar', 'baz'],
            // ['foo1', 'bar1', 'baz1'],
        ];

        
        
        //$file = "output/result".date("Y-m-d H:i:s").".csv";
        $file = "output/result.csv";
        
        $csv = Writer::createFromPath($file, "w");
        $csv->insertOne($header);
        foreach($finito as $row_data):
            $csv->insertOne($row_data);
        endforeach;
        //$csv->insertAll($recordz);
        echo 'iserted records';

        exit;

        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
        ]);
    }

    function disposition_classification($call_type){
        $call_type = strtolower(trim($call_type));
        $repository = $this->getDoctrine()
                ->getRepository(Dispositions::class);
        $dispositions = $repository->get_call_disposition($call_type);

        $this_disposition = '';

        foreach($dispositions as $disposition):
            $this_disposition = $disposition['parent_disposition'];
        endforeach;

        if($this_disposition == ''){
            $this_disposition = '15';
        }

        return $this_disposition;
    }

    function get_disposition($disposition_code){
        $disposition_code = strtolower(trim($disposition_code));

        $repository = $this->getDoctrine()
                ->getRepository(Dispositions::class);
        $dispositions = $repository->get_call_disposition_class($disposition_code);

        $this_disposition = '';

        foreach($dispositions as $disposition):
            $this_disposition = $disposition['disposition_name'];
        endforeach;

        return $this_disposition;
    }

}

<?php

namespace App\Repository;

use App\Entity\Dispositions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Dispositions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dispositions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dispositions[]    findAll()
 * @method Dispositions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispositionsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dispositions::class);
    }

    // /**
    //  * @return Dispositions[] Returns an array of Dispositions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dispositions
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function get_call_disposition($call_type){
        $conn = $this->getEntityManager()
            ->getConnection();

        $call_type = $conn->quote($call_type);

        $query = ('SELECT * FROM `crm_call_types` WHERE LOWER(description) = '.$call_type.';');
        
        $sth = $conn->prepare($query);
        $sth->execute();
        $results = $sth->fetchAll();

        return $results;
    }

    public function get_call_disposition_class($disposition_code){
        $conn = $this->getEntityManager()
            ->getConnection();

        $query = ('SELECT * FROM `dispositions` WHERE `record_id` = '.$disposition_code.';');
        
        $sth = $conn->prepare($query);
        $sth->execute();
        $results = $sth->fetchAll();

        return $results;
    }
}

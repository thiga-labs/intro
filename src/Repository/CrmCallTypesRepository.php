<?php

namespace App\Repository;

use App\Entity\CrmCallTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CrmCallTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method CrmCallTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method CrmCallTypes[]    findAll()
 * @method CrmCallTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CrmCallTypesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CrmCallTypes::class);
    }

    // /**
    //  * @return CrmCallTypes[] Returns an array of CrmCallTypes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CrmCallTypes
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
